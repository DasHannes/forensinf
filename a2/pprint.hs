{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE ScopedTypeVariables #-}

import qualified Data.ByteString.Lazy as B
import Control.Applicative
import Control.Monad 
import Control.Monad.IO.Class
import Control.Monad.Trans.State.Lazy

import Data.List.Split
import Data.Bits
import Data.Int
import Data.Word
import Data.List as L
import Data.Maybe
import qualified Data.Set as S
import qualified Data.Map as M

import System.IO

import Text.PrettyPrint.Boxes
import Text.Printf

import Debug.Trace

import System.Environment

import FSTypes

import GHC.Exts

blockSize = 512 
bootCodeLen = 446 :: Integer
entryLen = 16

-- | Turn list of byte values into corresponding little endian integer
littleInt bs = sum $ uncurry shift <$> zip (map fromIntegral bs) [0,8..] 

data ActiveFlag = Active | Inactive | Borked deriving (Show)
aFlag i = case i of 
  0x80 -> Active
  0x00 -> Inactive
  _    -> Borked

data DiskStretchKind = Primary | Extended | Logical | Unallocated | MBR | EBR  | Nil
  deriving( Show, Eq, Enum)
data DiskStretch = DiskStretch {
      dsType :: Word8, -- FIXME: replace with Maybe
      dsStart :: Int64,
      dsLength :: Int64,
      dsSlot :: (Int,Int), -- FIXME: replace with Maybe
      dsKind :: DiskStretchKind
    } 
nilExtPart = DiskStretch 0 0 0 (0,0) Nil

dsEnd ds = dsStart ds + dsLength ds

dsSizeBoxes ds = map text [show (dsStart ds), show (dsStart ds + dsLength ds - 1), show (dsLength ds)]
-- | Makes a list of boxes, one for each (non-ignored) part of a partition table entry of form: [slot, description, start, end, length]

dsBoxRow ds@(DiskStretch _ _ _ (_,slot_l) kind) | kind `elem` [MBR,EBR] = 
  map text [ "-", "Table #"++show slot_l, show kind] ++ dsSizeBoxes ds

dsBoxRow ds@(DiskStretch _ _ _ _ Unallocated) = 
  map text [ "-", "Unallocated", "-"] ++ dsSizeBoxes ds 

dsBoxRow ds@(DiskStretch typ _ _ (slot_h,slot_l) kind) =
  map text [ 
    show slot_h++":"++show slot_l, 
    show kind, 
    fsName typ ++ printf " (0x%02x)" typ
  ] ++ dsSizeBoxes ds

dsIsExtended = (flip elem) extTypes . dsType

numBoxs = map (text . show) [1,2..]
enumRows = zipWith (:) numBoxs

-- | Lays out a list of boxes in a left aligned table with one space column separation
tabular :: [[Box]] -> Box
tabular = hsep 3 right . map (vcat right) . L.transpose 

-- | Turns a list of the partition table bytes into a list of DiskStretch. Resulting DiskStretch values
-- | will have dsKind Extended if they're in extTypes, parameter "kind" otherwise.
mkStretch [ _, _, _, _, typ, _,_,_, lba0, lba1, lba2, lba3, len0, len1 ,len2 ,len3 ] slot kind =
  let
    chooseKind typ = if elem typ extTypes then Extended else kind
  in
    DiskStretch typ ( littleInt [lba0,lba1,lba2,lba3]) (littleInt [len0, len1, len2, len3]) slot (chooseKind typ)

mkBrEntries bs slot_h kind = 
  (\(bs,s,k) -> mkStretch bs s k) <$> zip3 (takeEntryBytes bs) [(slot_h,slot_l) | slot_l <- [0,1..]] [kind,kind..]

mkUnalloc start end = DiskStretch 0 start (end-start+1) (0,0) Unallocated

underline s =  s // text (replicate (cols s)  '-')
headRow = underline <$> text <$> ["Part.#", "Slot", "Kind", "Type", "Start", "End", "Length"] 

-- | Skips the boot code bytes, then takes n chunks of entryLen and returns them in lists
takeEntryBytes = takeWhile ((== entryLen) . length) . splitEvery entryLen . drop (fromIntegral bootCodeLen) . B.unpack

data STExtParse = STExtParse{
  -- | Set of visited [EM]BR block numbers, to avoid infinite recursion (actuall stackoverflows :/)
  -- | when encoutering cyclic partition table graphs.
  stVisited :: S.Set Int64,
  stHdl :: Handle,
  stBaseBlock :: Int64,
  -- | number of [EM]BRs already found.
  stCount :: Int
}

stInit hdl start = STExtParse{ 
  stHdl = hdl, 
  stBaseBlock = start, 
  stCount = 1, -- since MBR is alread done, FIXME: this is ugly
  stVisited = S.fromList [0]
}

type ExtParse = StateT STExtParse IO [DiskStretch]

-- | Recursivley follows the chain of EBRs and builds a [PEntry] describing
-- | the logical partitions. baseBlock is the logical partitions first block address
-- | relative to which all addresses are within it.
-- followExtParts :: Handle -> Int64 -> Int -> IO [DiskStretch] 
-- followExtParts hdl baseBlock brCount = do
followExtParts :: ExtParse
followExtParts = do
  
  -- fetch the parse state
  st@STExtParse{  
    stHdl = hdl,
    stBaseBlock = baseBlock ,
    stCount = brCount,
    stVisited = visited
  } <- get

  -- fetch file position and data
  curBlock :: Int64 <- liftIO $ hCurBlock hdl
  bs <- liftIO $ B.hGetContents hdl

  -- build entries from *only* the first *two* partition descriptor slots
  case take 4 $ mkBrEntries bs brCount Logical of
    [] -> return []
    ps@(_:_) -> do
      let ebr = DiskStretch 0 curBlock 1 (0,brCount) EBR
          entries = ebr : ( 
               (\part -> if (elem (dsType part) extTypes)
                           then part { dsStart = (dsStart part) + baseBlock,dsKind = Extended} -- logical partitions are relative to their EBR
                           else part { dsStart = (dsStart part) + curBlock} -- extended partitions are relative to the frist EBR
                ) <$> ps
            )
          -- find EBRs we haven't seen yet
          nextStarts :: [Int64] = filter (not . (flip S.member) visited) $ dsStart <$> (filter dsIsExtended entries) 
      
          -- recurse for each link entry, concatenate the recursively generated lists 
          childrenActions = (flip map) nextStarts $ \start -> trace ("ns "++show start) $ do
                              liftIO $ hGotoBlock hdl (fromIntegral start )
                              followExtParts

      -- store new state before recursing
      put st { 
        stCount = brCount + 1, 
        stVisited = S.insert curBlock visited
      }
      -- run the recursive actions
      children <- sequence childrenActions
      return  (entries ++ concat children)
  
-- | Recurse down the EBR chain, building up a list of DiskStretches.
-- | hdl must be positioned at an EBR.
findPartitions :: Handle -> IO [DiskStretch]
findPartitions hdl = do

  curBlock <- hCurBlock hdl
  let mbrStretch = DiskStretch 0 curBlock 1 (0,0) MBR

  bs <- B.hGetContents hdl
      -- parse entries, removing unused ones
  let pes = filter ((/= 0) . dsType) $ take 4 $ mkBrEntries bs 0 Primary
      -- find links to EBRs, FIXME: fail if there are more than one?
      exts = filter (((flip elem) extTypes) . dsType) pes

      -- start recursing down the EBR chain, if there is one
      findRest hdl (DiskStretch{ dsStart = start }) = do 
         hGotoBlock hdl start
         evalStateT followExtParts (stInit hdl start)
      findRest _ _ = return ([] :: [DiskStretch])

  rest <- sequence $ map (findRest hdl) exts
  return $ mbrStretch : concat (pes : rest)

-- | Intersperse a list of stretches with unallocated stretches where appropriate.
intersperseUnalloc :: [DiskStretch] -> [DiskStretch]
intersperseUnalloc (
      a@DiskStretch{ dsStart = astart, dsLength = alen} 
    : b@DiskStretch{ dsStart = bstart} 
    : rest
  ) | (astart+alen) < bstart = a : (mkUnalloc (astart+alen) (bstart-1)) : intersperseUnalloc (b:rest)

intersperseUnalloc (a:b:rest) = a : intersperseUnalloc (b:rest)
-- only (a:[]) and ([]) should match here
intersperseUnalloc listEnd = listEnd

-- | Moves a handle to an absolute (byte) position
hGoto :: Integral i => Handle -> i -> IO ()
hGoto h = hSeek h AbsoluteSeek . fromIntegral

hGotoBlock :: (Integral i) => Handle -> i -> IO ()
hGotoBlock h = hGoto h . (* blockSize) . fromIntegral

hCurBlock ::  Handle -> IO Int64 
hCurBlock h =  hTell h >>= return . (`div` blockSize) . fromIntegral

lastUsedBlock :: [DiskStretch] -> Int64
lastUsedBlock ps = maximum (map dsEnd ps)

main = do

  -- Open file given as first argument, crash if its not there. FIXME: don't crash
  fName <- liftA head getArgs 
  hdl <- openFile fName ReadMode
  hSeek hdl SeekFromEnd 0
  lastBlock <- hCurBlock hdl
  hSeek hdl AbsoluteSeek 0

  hSetBinaryMode hdl True

  -- Partition table entries don't have to be ordered in any way
  -- let's impose ordering by first block address
  -- aussming sortWith is stable, extended partitions and ebrs should still be in the 
  -- right places
  let sortStretches = sortWith dsStart 
  parts <- sortStretches . filter (\d -> (dsType d) /= 0x00 || elem (dsKind d) [EBR,MBR,Nil] ) <$> findPartitions hdl

  let partsEnd = lastUsedBlock parts
      lastEntry = if (partsEnd < lastBlock ) 
                    then mkUnalloc (partsEnd+1) lastBlock : [] -- stuff after last partition
                    else [] -- last partition fits neatly
      entries =  (intersperseUnalloc parts) ++ lastEntry -- put unallocs between non-consecutive tables

  -- Tabularized output of found entries
  printBox $ tabular ( headRow : enumRows (map dsBoxRow entries))
  hClose hdl
  

