{-# LANGUAGE ScopedTypeVariables #-}
module FSTypes where

import Data.Word
import Data.Maybe
import qualified Data.Map as M

extLBAType = 0x0f
extCHSType = 0x05
extTypes :: [Word8] = [extLBAType,extCHSType]

-- | Map from partitions/filesystem type ids to names
fsNames :: M.Map Word8 String
fsNames = M.fromList [
    (0x00,"Empty"),
    (0x01,"Fat12"),
    (0x04,"Fat16"),
    (0x05,"ExtCHS"),
    (0x0f,"ExtLBA"),
    (0x07,"NTFS"),
    (0x70,"DiskSecure"),
    (0x83,"Linux"),
    (0x82,"Swap")
  ]
fsName typ = fromMaybe  "Unknown" (M.lookup typ fsNames)
