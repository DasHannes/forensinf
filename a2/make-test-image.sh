#! /bin/sh

# create a sparse file

# add msdos partition table, then partitions, (unit == MB)
function create_img  {
  if [[ "" == "$1" ]]
  then
    echo "missing name for create_img"
    exit
  fi

  fname="$1"
  dd if=/dev/zero of="${fname}" seek=100G bs=1 count=1
  parted -s "${fname}" mklabel msdos
  parted -s "${fname}" mkpart p  ext2        1M     500M
  parted -s "${fname}" mkpart p  ntfs      501M    1020M 
  parted -s "${fname}" mkpart p  ntfs     1500M    5000M 
  parted -s "${fname}" mkpart e           5001M  100000M
  parted -s "${fname}" mkpart l  ext4     5002M    9000M
  parted -s "${fname}" mkpart l  ext4     9001M   25000M
  parted -s "${fname}" mkpart l  ext3    25001M   98000M
  parted -s "${fname}" mkpart l  ext2    98001M   99000M
  parted -s "${fname}" mkpart l  ntfs    99001M  100000M
  parted -s "${fname}" set 3 boot on
  parted -s "${fname}" print
  parted -s "${fname}" print
}

create_img "./test.img"
create_img "./test.trunc.img"
truncate -s $((193361792*512-1024*138)) "test.trunc.img"
