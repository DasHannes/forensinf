#include <inttypes.h>
#include <errno.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string.h>
#include <list>
#include <array>
#include <set>
#include <map>
#include <string>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

typedef array<uint8_t,2> JpegMarker;
const auto SOI = JpegMarker({0xFF,0xD8});
const auto EOI = JpegMarker({0xFF,0xD9});
const auto APP0 = JpegMarker({0xFF,0xE0});

// stand alone markers
const auto SaMarkers = [](){
  set< array<uint8_t,2>> s;
  s.insert( SOI); s.insert( EOI);
  // RST markers
  for( uint8_t i=0xD0; i <= 0xD7; ++i)
    s.insert( JpegMarker({0xFF,i}));
  return s;
}();

const auto JPEG_IN = array<uint8_t,6>({0xFF,0xD8,0xFF,0xE0,0x00,0x10});

const auto PNG_SIG = array<uint8_t,8>({137,80,78,71,13,10,26,10});
const auto IHDR = array<uint8_t,8>({0,0,0,0x0d,'I','H','D','R'});
const auto IEND = array<uint8_t,8>({0,0,0,0,'I','E','N','D'});

const auto PDF_IN = array<uint8_t,7>({'%','P','D','F','-','1','.'});
const auto PDF_OUT = array<uint8_t,5>({'%','%','E','O','F'});


template<typename T, size_t n>
bool match(const array<T,n> &prefix, T *data){
  for( T p : prefix)
    if( p != *(data++))
      return false;
  return true;
}

template<typename T, size_t n>
bool munch( const array<T,n> &prefix, T *(&data)){
  if( match( prefix, data)){
    data += n;
    return true;
  }
  return false;
}

enum FileType{ JPEG, PNG, PDF, FAIL};
const map<FileType,string> FileTypeNames = { {JPEG,"JPEG"}, {PNG,"PNG"}, {PDF,"PDF"}, {FAIL,"FAIL"}};
string FileTypeName( FileType ft){
  auto it = FileTypeNames.find( ft);
  if( it == FileTypeNames.end())
    return "Unknown";
  return it->second;
}

template<typename T, size_t n>
T bigEndian( array<uint8_t,n> bs){
  T v = 0;
  for( int i=0; i < n; ++i){
    v += (bs[i] << (8*(n-i-1)));
  }
  return v;
}

template<typename T, size_t n>
T littleEndian( array<uint8_t,n> bs){
	T v = 0;
	for( int i=0; i < n; ++i){
		v += (bs[i] << (8*i));
	}
	return v;
}

struct file{
	uint8_t *begin;
  size_t   len;
  FileType type;
  bool operator==( const file &o){
    return o.begin == begin && o.len == len && o.type == type;
  }
  bool operator!=(const file &o){
    return !(*this==o);
  }
};

ostream& operator<<( ostream &o, const FileType ft){
  o << FileTypeName( ft);
  return o;
}

ostream& operator<<( ostream &o, const file &f){
  o << "file{ " << (void*)f.begin << ", " << f.len << ", " << f.type << "}";
  return o;
}

ostream& operator<<( ostream &o, const JpegMarker &mark){
  o << "X'" << hex << (int) mark[0] << "." << hex << (int) mark[1];
  return o;
}

const file FailFile = {0,0,FAIL};

//FIXME: will run out of bounds
file carveJpeg(  uint8_t *data, uint8_t *end){
  file ret = {data,0,JPEG};
  
  if( !munch( SOI, data)) 
    return FailFile;
  if( !match( APP0, data))
    return FailFile;

	while (data - ret.begin + 1 < 1024*1024*5){
		// something doesn't work when we're just skipping segments (maybe non-contiguous streams?)
		// just skip some stuff, maybe we're lucky ...
		while( *data != 0xFF && data - ret.begin + 1 < 1024*1024*5){
			++data;
		}
		while( *data == 0xFF ){
			JpegMarker mark = {data[0], data[1]};
			munch( mark, data);

			if( mark == EOI){
				ret.len = data - ret.begin;
				return  ret;
			}

			if( mark == SOI){
				return FailFile;
			}

			if( SaMarkers.find( mark) != SaMarkers.end()){
				//cerr << "Found SA marker: " << mark << endl;
				continue;
			}

			size_t size = bigEndian<size_t,2>( { data[0], data[1]}); // (_size[1]<<8) + _size[0];
			data += size;
			// if size == 0 skip
			if( size < 2)
				data += 2;
		}
	}
  return FailFile;
}

file carvePng( uint8_t *data, uint8_t *end){
	file ret = {data,0,PNG};

	if( !munch(PNG_SIG, data))
		return FailFile;
	if( !match(IHDR, data))
		return FailFile;

	// at 50MiB, we probably missed something
	while(data - ret.begin < 1024*1024*50){
		if( match( IEND, data)){
			ret.len = data - ret.begin + 8;
			return ret;
		}
		size_t size = bigEndian<size_t,4>({data[0],data[1],data[2],data[3]});
		if( size > 1024*1024*50)
			return FailFile;
		// |name| + |size| + size + |crc|
		data += 4+4+size+4;
	}
	return FailFile;
}

file carvePdf( uint8_t *data, uint8_t *end){
	file ret = {data,0,PDF};

	if( !munch( PDF_IN, data))
		return FailFile;
	if( *data != '0' && (*data < '1' || *data > '7'))
		return FailFile;

	// i've seen ~ 300MiB PDFs, so 500 is not yet the safe side, but our images are only ~4GiB, so this is probably ok...
	// actually, thats too big :/
	while( data - ret.begin < 1024*1024*25){
		if( match(PDF_IN, data))
			return FailFile;
		if( munch(PDF_OUT, data)){
			ret.len = data - ret.begin + 1;
			return ret;
		}
		data ++;
	}
	return FailFile;
}


list<string> units = {"Byte","KiB","MiB","GiB","TiB"};
string human( size_t size){
	float fsz = size;
	auto it = units.begin();
	string unit = *it;
	while( fsz > 1024.0 && it != units.end()){
		unit = *(++it);
		fsz /= 1024.0;
	}
	stringstream ss;
	ss << fsz << " " << unit;
	return ss.str();
}

list<file> carve( uint8_t *data, size_t len){
	list<file> files;
	for( size_t i=0*len; i < len; ++i){

		if( i % 0x1000000 == 0)
			cerr << "@ " << human(i) << "/" << human(len) << "(" << (float)i/len *100.0 << "%)" << endl;
		if( match( JPEG_IN, data+i)){
			//cerr << "JPG_IN at " << i << endl;
			auto jpg = carveJpeg( data+i, data+len);
      if( jpg != FailFile){
        jpg.begin -= (size_t) data;
        cerr << "JPEG : " << jpg << endl;
				files.push_back( jpg);
      }
		}

		if( match( PNG_SIG, data+i)){
			auto png = carvePng( data+i, data+len);
			if( png != FailFile){
				png.begin -= (size_t) data;
				cerr << "PNG : " << png << endl;
				files.push_back( png);
			}
		}

		if( match( PDF_IN, data+i)){
			auto pdf = carvePdf( data+i, data+len);
			if( pdf != FailFile){
				pdf.begin -= (size_t) data;
				cerr << "PDF : " << pdf << endl;
				files.push_back( pdf);
			}
		}

  }
	return files;
}

int main(int argc, char **argv){
  if( argc != 2){
    cerr << "missing argument" << endl;
    abort();
  }

  int fd = open(argv[1],  O_RDONLY);
  if( fd < 0 ){
    cerr << "failed to open " << argv[1] << ": " << strerror( errno) << endl;
    abort();
  }


  struct stat s;
  fstat( fd, &s);
  size_t len = s.st_size;
    
  uint8_t *data = (uint8_t*)mmap( 0, len, PROT_READ, MAP_PRIVATE, fd, 0);
  if( data == 0){
    cerr << "mmap failed: " << strerror( errno) << endl;
    abort();
  }

	auto files = carve( data, len);
	for( file f : files){
		stringstream ss;
		ss << (size_t)f.begin << "." << FileTypeName( f.type);
		ofstream out;
		out.open(ss.str(),ios_base::out | ios_base::binary | ios_base::trunc);
		if( !out.is_open()){
			cerr << "couldn't open " << ss.str() << endl;
			continue;
		}
		else{
			cerr << "writing " << ss.str() << endl;
		}
		out.write( (char*)((size_t)f.begin+data), f.len);
		out.close();
	}
	return 24;
}

